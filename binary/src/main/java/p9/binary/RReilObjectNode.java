package p9.binary;

import javax.swing.Action;
import org.openide.loaders.DataNode;
import org.openide.nodes.Children;

/**
 * Visual presentation layer for RREIL objects as nodes.
 */
public class RReilObjectNode extends DataNode {
  private final String label;
  private final RReilFileDataObject file;

  public RReilObjectNode (RReilFileDataObject obj) {
    super(obj, Children.LEAF, obj.getLookup());
    file = obj;
    label = "<font color='!textText'>" + super.getDisplayName() + "</font>     " +
        "<font color='!controlDkShadow'><i>" + "RREIL Code" + "</i> " + "</font>";
  }

  @Override public String getHtmlDisplayName () {
    return label;
  }

  @Override public Action getPreferredAction () {
    return new OpenRReilAction(file);
  }

}
