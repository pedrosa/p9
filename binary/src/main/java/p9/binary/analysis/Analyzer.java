package p9.binary.analysis;

import rreil.lang.RReilAddr;
import bindead.analyses.Analysis;
import bindead.analyses.AnalysisFactory;
import bindead.analyses.algorithms.CallStringAnalysis;
import bindead.analyses.systems.GenericSystemModel;
import bindead.analyses.systems.SystemModelRegistry;
import bindead.environment.AnalysisEnvironment;
import binparse.Binary;
import binparse.rreil.RReilBinary;

/**
 * An utility class to initialize and run analyses.
 *
 * @author Bogdan Mihaila
 */
public class Analyzer {
  private static final String defaultDomainHierarchy = AnalysisFactory.defaultDomainHierarchy;
  private final Binary binary;
  private final Analysis<?> analysis;

  /**
   * Instantiate the analysis with the default domain hierarchy.
   */
  public Analyzer (String rreilAssembly) {
    this(RReilBinary.fromString(rreilAssembly), defaultDomainHierarchy);
  }

  /**
   * Instantiate the analysis with the default domain hierarchy.
   */
  public Analyzer (Binary binary) {
    this(binary, defaultDomainHierarchy);
  }

  /**
   * Instantiate the analysis with the domain hierarchy given by {@code domainHierarchy}.
   *
   * @param domainHierarchy A string to be parsed and instantiated to a domain hierarchy.
   * @see DomainFactory.parseFactory() for a description of the syntax.
   */
  @SuppressWarnings({"unchecked", "rawtypes"}) public Analyzer (Binary binary, String domainHierarchy) {
    this.binary = binary;
    GenericSystemModel systemModel = SystemModelRegistry.getLinuxModel(binary); // for now the only thing we have
    AnalysisEnvironment environment = new AnalysisEnvironment(systemModel, null);
    analysis = new CallStringAnalysis(environment, binary, AnalysisFactory.buildInitialDomain(domainHierarchy));
  }

  public Analysis<?> runAnalysis () {
    RReilAddr startPoint = AnalysisFactory.getStartAddress(binary);
    analysis.runFrom(startPoint);
    return analysis;
  }

  public Analysis<?> getAnalysis () {
    return analysis;
  }

  public Binary getBinary () {
    return binary;
  }
}
