
package p9.binary.cfg.display.items.instructions.operands.rreil;

import java.util.LinkedList;
import java.util.List;

import p9.binary.cfg.display.items.DrawableItem;
import p9.binary.cfg.display.items.TextItem;
import p9.binary.cfg.display.items.instructions.operands.OperandsItem;
import rreil.disassembler.Instruction;
import rreil.lang.Lhs;
import rreil.lang.RReil;
import rreil.lang.RReil.Assertion.AssertionCompare;
import rreil.lang.RReil.Assertion.AssertionReachable;
import rreil.lang.RReil.Assertion.AssertionUnreachable;
import rreil.lang.RReil.Assertion.AssertionWarnings;
import rreil.lang.Rhs;
import rreil.lang.lowlevel.RReilHighLevelToLowLevelWrapper;
import rreil.lang.util.RReilVisitor;
import static p9.binary.cfg.display.items.instructions.operands.rreil.RReilOperandItem.toItem;

/**
 *
 * @author Raphael Dümig
 */
public class RReilOperandsItem extends OperandsItem {
  private final boolean numbersInHex;
  private final RReil rreilInstr;

  public RReilOperandsItem(Instruction instr, boolean numbersInHex) {
    assert(instr instanceof RReilHighLevelToLowLevelWrapper);

    this.numbersInHex = numbersInHex;
    this.rreilInstr = ((RReilHighLevelToLowLevelWrapper) instr).toRReil();

    updateOperands();
  }


  @Override
  public String getOperandsString() {
    return getOperandsRReilInstructionType(rreilInstr, numbersInHex);
  }

  private static String getOperandsRReilInstructionType(RReil insn, @SuppressWarnings("unused") boolean numbersInHex) {
    RReilOperandsPrinter visitor = new RReilOperandsPrinter();
    return insn.accept(visitor, new StringBuilder()).toString();
  }

  private void updateOperands() {
    removeAllItems();
    // skip all operands if we have a halt instruction
    if(!rreilInstr.mnemonic().equalsIgnoreCase("halt"))
      addAllItems(rreilInstr.accept(new RReilOperandsGenerator(), numbersInHex));
  }



  /**
   * A dispatcher for RREIL instructions that retrieves the operands and prints
   * them as a comma-separated list.
   *
   * Moved from RReilInstructionRenderer to RReilOperandsItem by Raphael Dümig
   *
   * This class is NOT used anymore to display the output in the CFG, but is
   * still necessary in some places to get the instruction in ASCII-form
   * (i.e. to copy it to the clipboard). --- Raphael
   *
   * @author Bogdan Mihaila
   */
  private static class RReilOperandsPrinter implements RReilVisitor<StringBuilder, StringBuilder> {

    /**
     * The default implementation just splits the mnemonic off the instruction
     * string and returns the rest. This behavior is enough for most
     * instructions. Implement more sophisticated logic if needed.
     */
    private static StringBuilder defaultOperandsExtractor(RReil insn, StringBuilder builder) {
      String insnString = insn.toString();
      int firstSpace = insnString.indexOf(' ');
      assert firstSpace != 0 : "A RREIL instruction should start with a non-empty mnemonic.";
      if (firstSpace < 0) // no operands
        return builder;
      builder.append(insnString.substring(firstSpace).trim());
      return builder;
    }

    @Override
    public StringBuilder visit(RReil.Assign stmt, StringBuilder builder) {
      return defaultOperandsExtractor(stmt, builder);
    }

    @Override
    public StringBuilder visit(RReil.Load stmt, StringBuilder builder) {
      return defaultOperandsExtractor(stmt, builder);
    }

    @Override
    public StringBuilder visit(RReil.Store stmt, StringBuilder builder) {
      return defaultOperandsExtractor(stmt, builder);
    }

    @Override
    public StringBuilder visit(RReil.BranchToNative stmt, StringBuilder builder) {
      return defaultOperandsExtractor(stmt, builder);
    }

    @Override
    public StringBuilder visit(RReil.BranchToRReil stmt, StringBuilder builder) {
      return defaultOperandsExtractor(stmt, builder);
    }

    @Override
    public StringBuilder visit(RReil.Nop stmt, StringBuilder builder) {
      return defaultOperandsExtractor(stmt, builder);
    }

    @Override
    public StringBuilder visit(RReil.Assertion stmt, StringBuilder builder) {
      return defaultOperandsExtractor(stmt, builder);
    }

    @Override
    public StringBuilder visit(RReil.Branch stmt, StringBuilder builder) {
      return defaultOperandsExtractor(stmt, builder);
    }

    @Override
    public StringBuilder visit(RReil.PrimOp stmt, StringBuilder builder) {
      return defaultOperandsExtractor(stmt, builder);
    }

    @Override
    public StringBuilder visit(RReil.Native stmt, StringBuilder builder) {
      return defaultOperandsExtractor(stmt, builder);
    }

    @Override
    public StringBuilder visit(RReil.Throw stmt, StringBuilder builder) {
      return defaultOperandsExtractor(stmt, builder);
    }

    @Override
    public StringBuilder visit(RReil.Flop stmt, StringBuilder builder) {
      return defaultOperandsExtractor(stmt, builder);
    }

  }

  /**
   * A dispatcher for RReil instructions that retrieves the operands and generates
   * a list of RReilOperandItems that should be inserted in the RReilOperandsItem.
   *
   * @author Raphael Dümig <duemig@in.tum.de>
   */
  private static class RReilOperandsGenerator implements RReilVisitor<List<DrawableItem>, Boolean> {
    
    @Override
    public List<DrawableItem> visit(RReil.Assign stmt, Boolean numbersInHex) {
      List<DrawableItem> result = new LinkedList<>();

      Rhs rhs = stmt.getRhs();
      Lhs lhs = stmt.getLhs();

      if (rhs instanceof Rhs.Bin) {
        Rhs.Bin binexpr = (Rhs.Bin) rhs;
        Rhs.Rval par1 = binexpr.getLeft();
        Rhs.Rval par2 = binexpr.getRight();

        result.add(toItem(lhs.asRvar(), numbersInHex));
        result.add(toItem(par1, numbersInHex));
        result.add(toItem(par2, numbersInHex));
      }
      else if (rhs instanceof Rhs.Cmp) {
        Rhs.Cmp cmpexpr = (Rhs.Cmp) rhs;
        Rhs.Rval par1 = cmpexpr.getLeft();
        Rhs.Rval par2 = cmpexpr.getRight();

        result.add(toItem(lhs.asRvar(), numbersInHex));
        result.add(toItem(par1, numbersInHex));
        result.add(toItem(par2, numbersInHex));
      }
      else {
        result.add(toItem(lhs.asRvar(), numbersInHex));
        result.add(toItem(rhs, numbersInHex));
      }

      return result;
    }

    @Override
    public List<DrawableItem> visit(RReil.Load stmt, Boolean numbersInHex) {
      List<DrawableItem> result = new LinkedList<>();

      result.add(toItem(stmt.getLhs().asRvar(), numbersInHex));
      result.add(new RReilAddressOperandItem(stmt.getReadAddress(), numbersInHex));

      return result;
    }

    @Override
    public List<DrawableItem> visit(RReil.Store stmt, Boolean numbersInHex) {
      List<DrawableItem> result = new LinkedList<>();

      result.add(new RReilAddressOperandItem(stmt.getWriteAddress(), numbersInHex));
      result.add(toItem(stmt.getRhs(), numbersInHex));

      return result;
    }

    @Override
    public List<DrawableItem> visit(RReil.BranchToNative stmt, Boolean numbersInHex) {
      List<DrawableItem> result = new LinkedList<>();

      if(stmt.isConditionalBranch())
        result.add(toItem(stmt.getCond(), numbersInHex));

      result.add(new TextItem("goto native"));
      result.add(toItem(stmt.getTarget(), numbersInHex));

      return result;
    }

    @Override
    public List<DrawableItem> visit(RReil.BranchToRReil stmt, Boolean numbersInHex) {
      List<DrawableItem> result = new LinkedList<>();

      if(stmt.isConditionalBranch())
        result.add(toItem(stmt.getCond(), numbersInHex));

      result.add(new TextItem("goto rreil"));
      result.add(toItem(stmt.getTarget(), numbersInHex));

      return result;
    }

    @Override
    public List<DrawableItem> visit(RReil.Nop stmt, Boolean numbersInHex) {
      return new LinkedList<>();
    }

    @Override
    public List<DrawableItem> visit(RReil.Assertion stmt, Boolean numbersInHex) {
      LinkedList<DrawableItem> result = new LinkedList<>();

      if(stmt instanceof AssertionCompare) {
        AssertionCompare ac = (AssertionCompare) stmt;

        result.add(toItem(ac.getLhs(), numbersInHex));
        result.add(new TextItem(ac.getOperator().toString()));
        result.add(toItem(ac.getRhs(), numbersInHex));
      }
      else if(stmt instanceof AssertionReachable) {
        result.add(new TextItem("<reachable>"));
      }
      else if(stmt instanceof AssertionUnreachable) {
        result.add(new TextItem("<unreachable>"));
      }
      else if(stmt instanceof AssertionWarnings) {
        AssertionWarnings aw = (AssertionWarnings) stmt;

        result.add(new TextItem("#warnings ="));
        result.add(new TextItem(Integer.toString(aw.getNumberOfExpectedWarnings())));
      }

      return result;
    }

    @Override
    public List<DrawableItem> visit(RReil.Branch stmt, Boolean numbersInHex) {
      List<DrawableItem> result = new LinkedList<>();
      result.add(toItem(stmt.getTarget(), numbersInHex));

      return result;
    }

    @Override
    public List<DrawableItem> visit(RReil.PrimOp stmt, Boolean numbersInHex) {
      List<DrawableItem> result = new LinkedList<>();

      result.add(new RReilOutArgsOperandsItem(stmt.getOutArgs(), numbersInHex));
      result.add(new TextItem(" = "));
      result.add(new TextItem(stmt.getName()));
      result.add(new RReilInArgsOperandsItem(stmt.getInArgs(), numbersInHex));

      return result;
    }

    @Override
    public List<DrawableItem> visit(RReil.Native stmt, Boolean numbersInHex) {
      List<DrawableItem> result = new LinkedList<>();

      result.add(new TextItem("("));
      result.add(toItem(stmt.getOpnd(), numbersInHex));
      result.add(new TextItem(")"));
      result.add(new TextItem(":"));
      result.add(new TextItem(Integer.toString(stmt.getOpnd().getSize())));

      return result;
    }

    @Override
    public List<DrawableItem> visit(RReil.Throw stmt, Boolean numbersInHex) {
      LinkedList<DrawableItem> result = new LinkedList<>();
      result.add(new TextItem(stmt.getException()));
      return result;
    }

    @Override
    public List<DrawableItem> visit(RReil.Flop stmt, Boolean numbersInHex) {
      List<DrawableItem> result = new LinkedList<>();
      result.add(toItem(stmt.getLhs(), numbersInHex));

      RReilFlopOperandsItem flopOperands = new RReilFlopOperandsItem();
      for(Rhs rhs: stmt.getRhs())
        flopOperands.addItem(toItem(rhs, numbersInHex));

      result.add(flopOperands);

      RReilFlopFlagsItem flopFlags = new RReilFlopFlagsItem();
      flopFlags.addItem(toItem(stmt.getFlags(), numbersInHex));

      result.add(flopFlags);

      return result;
    }
  }
}
