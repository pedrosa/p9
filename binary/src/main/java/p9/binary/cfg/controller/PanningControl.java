package p9.binary.cfg.controller;

import java.awt.event.MouseEvent;

import prefuse.controls.PanControl;
import prefuse.visual.EdgeItem;
import prefuse.visual.VisualItem;

/**
 * A panning control that also pans when clicking on edges (but not on nodes).
 */
public class PanningControl extends PanControl {

  public PanningControl () {
    this(LEFT_MOUSE_BUTTON);
  }

  public PanningControl (int mouseButton) {
    super(mouseButton, false);
  }

  @Override public void itemPressed (VisualItem item, MouseEvent e) {
    if (item instanceof EdgeItem)
      mousePressed(e);
  }

  @Override public void itemDragged (VisualItem item, MouseEvent e) {
    if (item instanceof EdgeItem)
      mouseDragged(e);
  }

  @Override public void itemReleased (VisualItem item, MouseEvent e) {
    if (item instanceof EdgeItem)
      mouseReleased(e);
  }

}
