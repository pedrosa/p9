# P9 #
p9 is the GUI for the [Bindead binary analyzer](https://bitbucket.org/mihaila/bindead) developed at the Technical University of Munich. The GUI is used to invoke and display the analyzer results as Call Graphs, Control Flow Graphs (CFG) or as disassembly text. It is possible to interact with the CFGs to inspect the values of registers, search for strings, show all values inferred by the analysis for a program point or slice the CFG for a given register. More features and usage instructions are given on the [wiki of the project page](https://bitbucket.org/mihaila/p9/wiki/Home). 

[![GUI Screenshot](https://bitbucket.org/mihaila/p9/wiki/images/screenshot-small.png "GUI Screenshot - click for bigger size")](https://bitbucket.org/mihaila/p9/wiki/images/screenshot.png)

## Download and usage: ##
To download the sources clone the [p9 repository](https://bitbucket.org/mihaila/p9). Prebuild packages will be posted on the site in future.

### License ###
The project is open source using the GPL License. See LICENSE file for details.

### Development ###
Feel free to fork and send pull requests or report bugs in the issue tracker on the [project's site](https://bitbucket.org/mihaila/p9/issues). The project is developed with both Eclipse and Netbeans. A code-style file for these IDEs is available in the `resources` directory of the [Bindead project][bindead]. Although untested, the project should be loadable into any IDE that integrates with the Maven build tool or use you favorite editor and build from the command line.

### Contribution guidelines ###
Feel free to fork and send pull requests or report bugs in the issue tracker on the [project's site](https://bitbucket.org/mihaila/p9/issues). The project has is beeing developed with both Eclipse and Netbeans. However, the project can be loaded into any IDE that integrates with the Maven build tool. 

### Configuration ###
The configuration is stored in `$HOME/.p9` but is currently mostly about the state of the Netbeans GUI components.

## Building: ##
### Dependences ###
Some dependencies are either shipped with the code, like the [Prefuse graph library][prefuse] or will be automatically downloaded during the build process, like the [Netbeans GUI framework][netbeans]. The Bindead binary analyzer that the GUI depends on must currently be downloaded and build locally before being able to build the GUI. See the [Bindead][bindead] page for more information on building the analyzer. In the future the analyzer should be downloadable as a dependency library during the build.

### Requirements: ###
The GUI is written in Java, thus the dependencies are:

* JDK 1.7 or higher
* Maven build tool
* [Bindead analyzer](https://bitbucket.org/mihaila/bindead)

### Build process: ###

Build and install Bindead locally using the build instructions on the project page (e.g. `mvn install`). Then to build the GUI for it

under Unix: 

* use the shipped `build.sh` script in the top-level directory. It will build all the components and copy the resulting program to `p9.zip` to the current directory. Just unpack the result anywhere on your disk and run `p9/bin/p9`

under Windows and Unix or anything else:

* go into the `p9/prefuse/` subdirectory and build and locally install everything using `mvn install -DskipTests`
* go back into the main project directory `p9/` and build everything using `mvn install`
If everything went fine the whole project with dependencies is packaged in the directory `p9/application/target/p9`. There is also a zip file of that directory in `p9/application/target/` which can be used as a standalone package. Unpack the result anywhere on your disk and run `p9/bin/p9` under Unix or `p9/bin/p9.exe` (`p9/bin/p964.exe`) on Windows systems.

## Contact ##
Bogdan Mihaila <mihaila@in.tum.de>


[bindead]: https://bitbucket.org/mihaila/bindead "Bindead page"
[prefuse]: http://en.wikipedia.org/wiki/Prefuse "Prefuse Graph library"
[netbeans]: https://www.netbeans.org/ "Netbeans Application Framework"